const express=require('express')
const router=express.Router();
const userControllers=require('../controllers/userControllers')

router.post('/checkEmail',userControllers.checkEmailExists)
router.post('/register',userControllers.registerUser)
router.post('/login',userControllers.loginUser)
router.post('/details',userControllers.userDetails)

module.exports=router;