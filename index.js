// *setup dependencies
const express=require('express')
const mongoose=require('mongoose')
const cors=require('cors')
const userRoutes=require('./routes/userRoutes')

const app=express()
// *middlewares
app.use(cors())
app.use(express())
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use('/users',userRoutes)

mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.e2dwbki.mongodb.net/bookingAPI")
// prompt message to terminal
mongoose.connection.on('error',()=>console.error.bind(console, "connection error"))
mongoose.connection.once('open',()=>console.log("Now connected to mongoDB Atlas"))

app.listen(process.env.PORT || 4000,()=>{
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
})